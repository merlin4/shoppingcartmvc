﻿using ShoppingCartMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShoppingCartMVC.Controllers
{
    public class CartController : Controller
    {
        private HalloweenDatabase db = new HalloweenDatabase();
        
        public ActionResult Index()
        {
            var cart = ShoppingCart.Load(this.HttpContext);
            return View(cart);
        }

        public ActionResult Add()
        {
            LoadProducts();
            return View(new Order());
        }

        [HttpPost]
        public ActionResult Add([Bind(Include = "ProductID,Quantity")] Order order)
        {
            LoadProducts();
            if (ModelState.IsValid)
            {
                var cart = ShoppingCart.Load(this.HttpContext);
                var product = db.Products.Find(order.ProductID);
                order.ProductName = product.Name;

                var prevOrder = cart.Orders.FirstOrDefault(o => o.ProductID == order.ProductID);
                if (prevOrder == null)
                {
                    cart.Orders.Add(order);
                }
                else
                {
                    prevOrder.Quantity += order.Quantity;
                }

                cart.TotalPrice += order.Quantity.Value * product.UnitPrice;
                return RedirectToAction("Index", "Cart");
            }
            return View(order);
        }

        private void LoadProducts()
        {
            var products = db.Products.Select(
                p => new { p.ProductID, p.Name }
            );
            ViewBag.Products = new SelectList(
                products, "ProductID", "Name"
            );
        }

        public ActionResult GetProduct(string id)
        {
            var product = db.Products.Find(id);
            return Json(product, JsonRequestBehavior.AllowGet);
        }
    }
}