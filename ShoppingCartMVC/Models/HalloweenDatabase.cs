﻿using System.Data.Entity;

namespace ShoppingCartMVC.Models
{
    public class HalloweenDatabase : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}