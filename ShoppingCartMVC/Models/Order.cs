﻿using System.ComponentModel.DataAnnotations;

namespace ShoppingCartMVC.Models
{
    public class Order
    {
        [Required]
        [Display(Name="Product")]
        public string ProductID { get; set; }

        public string ProductName { get; set; }

        [Required, Range(1, 999)]
        public int? Quantity { get; set; }

        public override string ToString()
        {
            return string.Format("{0} ({1})", ProductName, Quantity);
        }
    }
}