﻿using System.ComponentModel.DataAnnotations;

namespace ShoppingCartMVC.Models
{
    public class Product
    {
        [Key, MaxLength(10), Required]
        public string ProductID { get; set; }

        [MaxLength(50), Required]
        public string Name { get; set; }

        [MaxLength(200), Required]
        public string ShortDescription { get; set; }

        [MaxLength(2000), Required]
        public string LongDescription { get; set; }

        [MaxLength(10), Required]
        public string CategoryID { get; set; }

        [DataType(DataType.ImageUrl), MaxLength(30)]
        public string ImageFile { get; set; }

        [DataType(DataType.Currency), Required]
        public decimal UnitPrice { get; set; }

        [Required]
        public int OnHand { get; set; }
}
}