﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ShoppingCartMVC.Models
{
    public class ShoppingCart
    {
        public List<Order> Orders { get; set; }

        [Display(Name = "Total Price")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal TotalPrice { get; set; }

        public ShoppingCart()
        {
            Orders = new List<Order>();
            TotalPrice = 0;
        }

        public static ShoppingCart Load(HttpContextBase context)
        {
            var session = context.Session;
            var cart = session["ShoppingCart"] as ShoppingCart;
            if (cart == null)
            {
                session["ShoppingCart"] = new ShoppingCart();
                cart = session["ShoppingCart"] as ShoppingCart;
            }
            return cart;
        }
    }
}